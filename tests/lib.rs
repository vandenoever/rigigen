extern crate rigigen;

use rigigen::generate;
use std::path::Path;
use std::io;
use std::io::{Read,Write};
use std::fs::File;

fn test_generate(in_path: &str, out_path: &str) -> io::Result<()> {
    let mut i = try!(File::open(in_path));
    let mut o = vec!();
    generate(&mut i, &mut o);
    let mut r = vec!();
    try!(File::open(out_path).unwrap().read_to_end(&mut r));
    assert_eq!(o, r);
    Ok(())
}


#[test]
fn tar() {
    test_generate("tests/tar.xml", "tests/tar.rs").unwrap();
}
