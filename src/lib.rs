extern crate xml;

use std::path::Path;
use std::io;
use std::io::{Read,Write};
use std::fs::File;

mod parse_dsdl;

pub fn generate_file(in_path: &Path, out_path: &Path) -> io::Result<()> {
    let mut i = try!(File::open(in_path));
    let mut o = try!(File::create(out_path));
    generate(&mut i, &mut o)
}

pub fn generate(input: &mut Read, output: &Write) -> io::Result<()> {
    let dsdl = parse_dsdl::parse(input);
    Ok(())
}
