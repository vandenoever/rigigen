extern crate xml;

use std::io;
use std::io::BufReader;
use std::io::Read;
use xml::name::OwnedName;
use xml::common::TextPosition;
use xml::reader::{EventReader, XmlEvent, Result, Error, ErrorKind};

const XSDNS: &'static str = "http://www.w3.org/2001/XMLSchema";
const DFDLNS: &'static str = "http://www.ogf.org/dfdl/dfdl-1.0/";

fn is_xsd(local_name: &str, name: OwnedName) -> bool {
    name.namespace.is_some() &&
         name.namespace.unwrap() == XSDNS && name.local_name == local_name
}

pub struct Type {
}

pub struct Dsdl {
    types: Vec<Type>
}

const ERR: Error = Error{pos: TextPosition{row:0, column:0}, kind: ErrorKind::UnexpectedEof};

fn parse_complex_type<R:Read>(reader: &mut EventReader<R>) -> Result<Type> {
    loop {
        let e = reader.next();
        match e {
            Err(e) => return Err(e),
            Ok(XmlEvent::StartElement { name, .. }) => {
                assert!(is_xsd("complexType", name));
            },
            Ok(XmlEvent::EndElement{..}) => break,
            _ => {}
        }
    }
    Err(ERR)
}

fn parse_types<R:Read>(reader: &mut EventReader<R>) -> Result<Vec<Type>> {
    loop {
        let e = reader.next();
        match e {
            Err(e) => return Err(e),
            Ok(XmlEvent::StartElement { name, .. }) => {
                assert!(is_xsd("complexType", name));
                parse_complex_type(reader);
            }
            Ok(XmlEvent::EndElement{..}) => break,
            _ => {}
        }
    }
    Err(ERR)
}

fn parse_schema<R:Read>(reader: &mut EventReader<R>) -> Result<Dsdl> {
    while let Ok(event) = reader.next() {
    }
    loop {
        let e = reader.next();
        match e {
            Ok(XmlEvent::StartElement { name, .. }) => {
                assert!(is_xsd("schema", name));
                let dsdl = try!(parse_types(reader));
                return Ok(Dsdl{types:dsdl});
            },
            Ok(XmlEvent::EndDocument) | Err(_) => break,
            _ => {}
        }
    }
    Err(ERR)
}

pub fn parse(input: &mut Read) -> Result<Dsdl> {
    let input = BufReader::new(input);
    let mut parser = EventReader::new(input);
    parse_schema(&mut parser)
}
